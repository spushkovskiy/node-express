const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const homeRouts = require("./routes/home");
const coursesRouts = require("./routes/courses");
const addRouts = require("./routes/add");

const app = express();
const hbs = exphbs.create({
  defaultLayout: "main",
  extname: "hbs"
});

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
app.set("views", "views");

app.use(express.static("public"));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use("/", homeRouts);
app.use("/courses", coursesRouts);
app.use("/add", addRouts);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

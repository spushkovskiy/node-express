const { Router } = require("express");
const Course = require("../model/course");

const router = Router();

router.get("/", async (req, res) => {
  const courses = await Course.getAll();
  res.render("courses", {
    title: "Courses",
    isCourse: true,
    courses
  });
});

module.exports = router;
